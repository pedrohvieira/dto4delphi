unit DTO.SementesPilha;

interface

uses
  Types.Attributes.DTO4Delphi;
{$M+}

type

  [VsTable('SEPILHA'), VsQuery('SEL_DTO_SEPILHA'), VsCompositeKey]
  IDTOSementesPilha = interface

    ['{2CC57650-1772-481A-B6E4-F101DE0508D9}']

    [VsField('ESTAB'), VsKey]
    function SetEstabelecimento(AValue: Integer): IDTOSementesPilha;
    function Estabelecimento: Integer;

    [VsField('CODIGO'), VsKey]
    function SetCodigo(AValue: Integer): IDTOSementesPilha;
    function Codigo: Integer;

    [VsField('ARMAZEM')]
    function SetArmazem(AValue: Integer): IDTOSementesPilha;
    function Armazem: Integer;

    [VsField('IDENTIFICACAO')]
    function SetIdentificacao(AValue: String): IDTOSementesPilha;
    function Identificacao: String;

    [VsField('LINHA')]
    function SetLinha(AValue: Integer): IDTOSementesPilha;
    function Linha: Integer;

    [VsField('SEQNALINHA')]
    function SetColuna(AValue: Integer): IDTOSementesPilha;
    function Coluna: Integer;

    [VsField('ITEM')]
    function SetItem(AValue: Integer): IDTOSementesPilha;
    function Item: Integer;

    [VsField('CULTIVAR')]
    function SetCultivar(AValue: Integer): IDTOSementesPilha;
    function Cultivar: Integer;

    [VsField('CATEGORIA')]
    function SetCategoria(AValue: String): IDTOSementesPilha;
    function Categoria: String;

    [VsField('PENEIRA')]
    function SetPeneira(AValue: String): IDTOSementesPilha;
    function Peneira: String;

    [VsField('TRATAMENTO')]
    function SetTratamento(AValue: Integer): IDTOSementesPilha;
    function Tratamento: Integer;

    [VsField('EMBALAGEM')]
    function SetEmbalagem(AValue: Integer): IDTOSementesPilha;
    function Embalagem: Integer;

    [VsField('QTDMAXLOTES')]
    function SetQuantidadeMaxLotes(AValue: Integer): IDTOSementesPilha;
    function QuantidadeMaxLotes: Integer;

    [VsField('ATIVA')]
    function SetAtiva(AValue: String): IDTOSementesPilha;
    function Ativa: String;

    [VsField('COR')]
    function SetCor(AValue: Integer): IDTOSementesPilha;
    function Cor: Integer;

    [VsField('OBSERVACAO')]
    function SetObservacao(AValue: String): IDTOSementesPilha;
    function Observacao: String;

    [VsField('IDCOR')]
    function SetIdCor(AValue: Integer): IDTOSementesPilha;
    function IdCor: Integer;

    [VsField('SUBLOCALARMAZEM')]
    function SetSubLocal(AValue: Integer): IDTOSementesPilha;
    function SubLocal: Integer;

    [VsField('CONFPILHA')]
    function SetConfiguracao(AValue: Integer): IDTOSementesPilha;
    function Configuracao: Integer;

  end;

  TDTOSementesPilha = class(TInterfacedObject, IDTOSementesPilha)
  private
    FEstabelecimento: Integer;
    FCodigo: Integer;
    FArmazem: Integer;
    FIdentificacao: String;
    FLinha: Integer;
    FColuna: Integer;
    FItem: Integer;
    FCultivar: Integer;
    FCategoria: String;
    FPeneira: String;
    FTratamento: Integer;
    FEmbalagem: Integer;
    FQuantidadeMaxLotes: Integer;
    FAtiva: String;
    FCor: Integer;
    FObservacao: String;
    FIdCor: Integer;
    FSubLocal: Integer;
    FConfiguracao: Integer;
  public
    constructor Create;
    destructor Destroy; override;

    [VsNew]
    class function New: IDTOSementesPilha;

    function SetEstabelecimento(AValue: Integer): IDTOSementesPilha;
    function Estabelecimento: Integer;

    function SetCodigo(AValue: Integer): IDTOSementesPilha;
    function Codigo: Integer;

    function SetArmazem(AValue: Integer): IDTOSementesPilha;
    function Armazem: Integer;

    function SetIdentificacao(AValue: String): IDTOSementesPilha;
    function Identificacao: String;

    function SetLinha(AValue: Integer): IDTOSementesPilha;
    function Linha: Integer;

    function SetColuna(AValue: Integer): IDTOSementesPilha;
    function Coluna: Integer;

    function SetItem(AValue: Integer): IDTOSementesPilha;
    function Item: Integer;

    function SetCultivar(AValue: Integer): IDTOSementesPilha;
    function Cultivar: Integer;

    function SetCategoria(AValue: String): IDTOSementesPilha;
    function Categoria: String;

    function SetPeneira(AValue: String): IDTOSementesPilha;
    function Peneira: String;

    function SetTratamento(AValue: Integer): IDTOSementesPilha;
    function Tratamento: Integer;

    function SetEmbalagem(AValue: Integer): IDTOSementesPilha;
    function Embalagem: Integer;

    function SetQuantidadeMaxLotes(AValue: Integer): IDTOSementesPilha;
    function QuantidadeMaxLotes: Integer;

    function SetAtiva(AValue: String): IDTOSementesPilha;
    function Ativa: String;

    function SetCor(AValue: Integer): IDTOSementesPilha;
    function Cor: Integer;

    function SetObservacao(AValue: String): IDTOSementesPilha;
    function Observacao: String;

    function SetIdCor(AValue: Integer): IDTOSementesPilha;
    function IdCor: Integer;

    function SetSubLocal(AValue: Integer): IDTOSementesPilha;
    function SubLocal: Integer;

    function SetConfiguracao(AValue: Integer): IDTOSementesPilha;
    function Configuracao: Integer;
  end;

implementation

uses Core.Registry.DTO4D;

{ TDTOSementesPilha }

function TDTOSementesPilha.Armazem: Integer;
begin
  Result := FArmazem;
end;

function TDTOSementesPilha.Ativa: String;
begin
  Result := FAtiva;
end;

function TDTOSementesPilha.Categoria: String;
begin
  Result := FCategoria;
end;

function TDTOSementesPilha.Codigo: Integer;
begin
  Result := FCodigo;
end;

function TDTOSementesPilha.Coluna: Integer;
begin
  Result := FColuna;
end;

function TDTOSementesPilha.Configuracao: Integer;
begin
  Result := FConfiguracao;
end;

function TDTOSementesPilha.Cor: Integer;
begin
  Result := FCor;
end;

constructor TDTOSementesPilha.Create;
begin
  inherited Create;
end;

function TDTOSementesPilha.Cultivar: Integer;
begin
  Result := FCultivar;
end;

destructor TDTOSementesPilha.Destroy;
begin

  inherited;
end;

function TDTOSementesPilha.Embalagem: Integer;
begin
  Result := FEmbalagem;
end;

function TDTOSementesPilha.Estabelecimento: Integer;
begin
  Result := FEstabelecimento;
end;

function TDTOSementesPilha.IdCor: Integer;
begin
  Result := FIdCor;
end;

function TDTOSementesPilha.Identificacao: String;
begin
  Result := FIdentificacao;
end;

function TDTOSementesPilha.Item: Integer;
begin
  Result := FItem;
end;

function TDTOSementesPilha.Linha: Integer;
begin
  Result := FLinha;
end;

class function TDTOSementesPilha.New: IDTOSementesPilha;
begin
  Result := Create;
end;

function TDTOSementesPilha.Observacao: String;
begin
  Result := FObservacao;
end;

function TDTOSementesPilha.Peneira: String;
begin
  Result := FPeneira;
end;

function TDTOSementesPilha.QuantidadeMaxLotes: Integer;
begin
  Result := FQuantidadeMaxLotes;
end;

function TDTOSementesPilha.SetArmazem(AValue: Integer): IDTOSementesPilha;
begin
  FArmazem := AValue;
end;

function TDTOSementesPilha.SetAtiva(AValue: String): IDTOSementesPilha;
begin
  FAtiva := AValue;
end;

function TDTOSementesPilha.SetCategoria(AValue: String): IDTOSementesPilha;
begin
  FCategoria := AValue;
end;

function TDTOSementesPilha.SetCodigo(AValue: Integer): IDTOSementesPilha;
begin
  FCodigo := AValue;
end;

function TDTOSementesPilha.SetColuna(AValue: Integer): IDTOSementesPilha;
begin
  FColuna := AValue;
end;

function TDTOSementesPilha.SetConfiguracao(AValue: Integer): IDTOSementesPilha;
begin
  FConfiguracao := AValue;
end;

function TDTOSementesPilha.SetCor(AValue: Integer): IDTOSementesPilha;
begin
  FCor := AValue;
end;

function TDTOSementesPilha.SetCultivar(AValue: Integer): IDTOSementesPilha;
begin
  FCultivar := AValue;
end;

function TDTOSementesPilha.SetEmbalagem(AValue: Integer): IDTOSementesPilha;
begin
  FEmbalagem := AValue;
end;

function TDTOSementesPilha.SetEstabelecimento(
  AValue: Integer): IDTOSementesPilha;
begin
  FEstabelecimento := AValue;
end;

function TDTOSementesPilha.SetIdCor(AValue: Integer): IDTOSementesPilha;
begin
  FIdCor := AValue;
end;

function TDTOSementesPilha.SetIdentificacao(AValue: String): IDTOSementesPilha;
begin
  FIdentificacao := AValue;
end;

function TDTOSementesPilha.SetItem(AValue: Integer): IDTOSementesPilha;
begin
  FItem := AValue;
end;

function TDTOSementesPilha.SetLinha(AValue: Integer): IDTOSementesPilha;
begin
  FLinha := AValue;
end;

function TDTOSementesPilha.SetObservacao(AValue: String): IDTOSementesPilha;
begin
  FObservacao := AValue;
end;

function TDTOSementesPilha.SetPeneira(AValue: String): IDTOSementesPilha;
begin
  FPeneira := AValue;
end;

function TDTOSementesPilha.SetQuantidadeMaxLotes(
  AValue: Integer): IDTOSementesPilha;
begin
  FQuantidadeMaxLotes := AValue;
end;

function TDTOSementesPilha.SetSubLocal(AValue: Integer): IDTOSementesPilha;
begin
  FSubLocal := AValue;
end;

function TDTOSementesPilha.SetTratamento(AValue: Integer): IDTOSementesPilha;
begin
  FTratamento := AValue;
end;

function TDTOSementesPilha.SubLocal: Integer;
begin
  Result := FSubLocal;
end;

function TDTOSementesPilha.Tratamento: Integer;
begin
  Result := FTratamento;
end;

initialization

TRegisterClassDTO4D(TRegisterClassDTO4D.GetGUID<IDTOSementesPilha>,
  TDTOSementesPilha);

finalization

end.
