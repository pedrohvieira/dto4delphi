object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 429
  ClientWidth = 408
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 104
    Top = 24
    Width = 193
    Height = 57
    Caption = 'Instanciar DTO'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 104
    Top = 96
    Width = 193
    Height = 57
    Caption = 'DataSet para Lista'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 264
    Width = 385
    Height = 157
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
  end
  object Button3: TButton
    Left = 104
    Top = 176
    Width = 193
    Height = 57
    Caption = 'DataSet para Objeto'
    TabOrder = 3
    OnClick = Button3Click
  end
  object memDataSet: TdxMemData
    Active = True
    Indexes = <>
    Persistent.Data = {
      5665728FC2F5285C8FFE3F130000000400000003000600455354414200040000
      0003000700434F4449474F00040000000300080041524D415A454D001E000000
      01000E004944454E54494649434143414F0004000000030006004C494E484100
      0400000003000B005345514E414C494E48410004000000030005004954454D00
      040000000300090043554C5449564152000500000001000A0043415445474F52
      4941000A0000000100080050454E45495241000400000003000B005452415441
      4D454E544F000400000003000A00454D42414C4147454D000400000003000C00
      5154444D41584C4F544553000100000001000600415449564100040000000300
      0400434F52006400000001000B004F42534552564143414F0004000000030006
      004944434F520004000000030010005355424C4F43414C41524D415A454D0004
      00000003000A00434F4E4650494C48410001F501000001010000000101000000
      0102000000303101010000000101000000017702000001030000000101000000
      420105000000554E4943410000000000010C000000010400000001010000004E
      0100FFFF000001040000000106000000010200000001F5010000010200000001
      0100000001020000003032010100000001020000000177020000010300000001
      0200000043310105000000554E4943410000000000010C000000010400000001
      010000004E0100FFFF000001040000000106000000010200000001F501000001
      0300000001010000000102000000303301010000000103000000016F02000001
      02000000010200000043320105000000554E4943410000000000010D00000001
      0600000001010000004E0100FFFF0000010700000001060000000102000000}
    SortOptions = []
    Left = 336
    Top = 128
    object fieldIntDataSetESTAB: TIntegerField
      FieldName = 'ESTAB'
    end
    object fieldIntDataSetCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object fieldIntDataSetARMAZEM: TIntegerField
      FieldName = 'ARMAZEM'
    end
    object fieldStrDataSetIDENTIFICACAO: TStringField
      FieldName = 'IDENTIFICACAO'
      Size = 30
    end
    object fieldIntDataSetLINHA: TIntegerField
      FieldName = 'LINHA'
    end
    object fieldIntDataSetSEQNALINHA: TIntegerField
      FieldName = 'SEQNALINHA'
    end
    object fieldIntDataSetITEM: TIntegerField
      FieldName = 'ITEM'
    end
    object fieldIntDataSetCULTIVAR: TIntegerField
      FieldName = 'CULTIVAR'
    end
    object fieldStrDataSetCATEGORIA: TStringField
      FieldName = 'CATEGORIA'
      Size = 5
    end
    object fieldStrDataSetPENEIRA: TStringField
      FieldName = 'PENEIRA'
      Size = 10
    end
    object fieldIntDataSetTRATAMENTO: TIntegerField
      FieldName = 'TRATAMENTO'
    end
    object fieldIntDataSetEMBALAGEM: TIntegerField
      FieldName = 'EMBALAGEM'
    end
    object fieldIntDataSetQTDMAXLOTES: TIntegerField
      FieldName = 'QTDMAXLOTES'
    end
    object fieldStrDataSetATIVA: TStringField
      FieldName = 'ATIVA'
      Size = 1
    end
    object fieldIntDataSetCOR: TIntegerField
      FieldName = 'COR'
    end
    object fieldStrDataSetOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 100
    end
    object fieldIntDataSetIDCOR: TIntegerField
      FieldName = 'IDCOR'
    end
    object fieldIntDataSetSUBLOCALARMAZEM: TIntegerField
      FieldName = 'SUBLOCALARMAZEM'
    end
    object fieldIntDataSetCONFPILHA: TIntegerField
      FieldName = 'CONFPILHA'
    end
  end
  object dsDados: TDataSource
    DataSet = memDataSet
    Left = 336
    Top = 184
  end
end
