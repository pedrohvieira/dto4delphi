unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls,
  DTO.Contracts.Samples,
  Core.Contracts.DTO4Delphi,
  Data.DB,
  Datasnap.DBClient, DTO.SementesPilha, dxmdaset;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    Button3: TButton;
    memDataSet: TdxMemData;
    fieldIntDataSetESTAB: TIntegerField;
    fieldIntDataSetCODIGO: TIntegerField;
    fieldIntDataSetARMAZEM: TIntegerField;
    fieldStrDataSetIDENTIFICACAO: TStringField;
    fieldIntDataSetLINHA: TIntegerField;
    fieldIntDataSetSEQNALINHA: TIntegerField;
    fieldIntDataSetITEM: TIntegerField;
    fieldIntDataSetCULTIVAR: TIntegerField;
    fieldStrDataSetCATEGORIA: TStringField;
    fieldStrDataSetPENEIRA: TStringField;
    fieldIntDataSetTRATAMENTO: TIntegerField;
    fieldIntDataSetEMBALAGEM: TIntegerField;
    fieldIntDataSetQTDMAXLOTES: TIntegerField;
    fieldStrDataSetATIVA: TStringField;
    fieldIntDataSetCOR: TIntegerField;
    fieldStrDataSetOBSERVACAO: TStringField;
    fieldIntDataSetIDCOR: TIntegerField;
    fieldIntDataSetSUBLOCALARMAZEM: TIntegerField;
    fieldIntDataSetCONFPILHA: TIntegerField;
    dsDados: TDataSource;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
    FSementesPilha: IDTOSementesPilha;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  Core.Manager.DTO4Delphi;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  FSementesPilha := TCoreManagerDTO4Delphi<IDTOSementesPilha>.New.DTO;

  FSementesPilha
    .SetEstabelecimento(501)
    .SetCodigo(4)
    .SetArmazem(5)
    .SetIdentificacao('04');

  ShowMessage(Format('|Estabelecimento %d|Codigo: %d|Armazem: %d|Identificação: %s|',
      [FSementesPilha.Estabelecimento, FSementesPilha.Codigo, FSementesPilha.Armazem, FSementesPilha.Identificacao]));
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  LSementesPilhaDAO: ICoreDTO4Delphi<IDTOSementesPilha>;
  LSementesPilhaDTO: IDTOSementesPilha;
begin
  Memo1.Lines.Clear;
  LSementesPilhaDAO := TCoreManagerDTO4Delphi<IDTOSementesPilha>.New.Params.DataSet
    (dsDados).Return;

  for LSementesPilhaDTO in LSementesPilhaDAO.DataSetToList do
  begin
    Memo1.Lines.Add(Format('|Estabelecimento %d|Codigo: %d|Armazem: %d|Identificação: %s|',
      [LSementesPilhaDTO.Estabelecimento, LSementesPilhaDTO.Codigo, LSementesPilhaDTO.Armazem, LSementesPilhaDTO.Identificacao]));
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  LSementesPilhaDTO: IDTOSementesPilha;
begin
  memDataSet.RecNo := 2;

  Memo1.Lines.Clear;

  LSementesPilhaDTO := TCoreManagerDTO4Delphi<IDTOSementesPilha>.New.Params.DataSet
    (dsDados).Return.DataSetToObject;

  Memo1.Lines.Add(Format('|Estabelecimento %d|Codigo: %d|Armazem: %d|Identificação: %s|',
      [LSementesPilhaDTO.Estabelecimento, LSementesPilhaDTO.Codigo, LSementesPilhaDTO.Armazem, LSementesPilhaDTO.Identificacao]));
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Memo1.Lines.Clear;
  ReportMemoryLeaksOnShutdown := True;
end;

end.
