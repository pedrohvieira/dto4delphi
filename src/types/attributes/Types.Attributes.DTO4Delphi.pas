unit Types.Attributes.DTO4Delphi;

interface

uses Types.Enums.DTO4D;

type
{$M+}
  VsNew          = class(TCustomAttribute) end; //Atributo de classe
  VsSimpleKey    = class(TCustomAttribute) end; //Atributo de interface que diz que a entidade tem um chave simples
  VsCompositeKey = class(TCustomAttribute) end; //Atributo de interface que diz que a entidade tem uma chave composta
  VsKey          = class(TCustomAttribute) end; //Atributo de function que diz que o field faz parte da chave da tabela
  VsNotUpdate    = class(TCustomAttribute) end; //Atributo de function que informa que o field n�o ser� considerado no CRUD

  VsTable = class(TCustomAttribute)
  private
    FTable: String;
    procedure SetTable(const Value: String);
  published
    constructor Create(ATable: String);
    destructor Destroy; override;
    property Field: String read FTable write FTable;
  end;

  VsField = class(TCustomAttribute)
  private
    FField: String;
    procedure SetField(const Value: String);
  published
    constructor Create(AName: String);
    destructor Destroy; override;
    property Field: String read FField write SetField;
  end;

  VsQuery = class(TCustomAttribute)
  private
    FQuerName: String;
    FQueryType: TEnumQuery;
    procedure SetQuerName(const Value: String);
    procedure SetQueryType(const Value: TEnumQuery);
  public
    constructor Create(const AQueryName: String;
      const AType: TEnumQuery = tpQueryBLOB);
    destructor Destroy; override;
  published
    property QuerName: String read FQuerName write SetQuerName;
    property QueryType: TEnumQuery read FQueryType write SetQueryType;
  end;

implementation

{ VsField }

constructor VsField.Create(AName: String);
begin
  Field := AName;
end;

destructor VsField.Destroy;
begin

  inherited;
end;

procedure VsField.SetField(const Value: String);
begin
  FField := Value;
end;

{ VsQuery }

constructor VsQuery.Create(const AQueryName: String;
  const AType: TEnumQuery = tpQueryBLOB);
begin
  QuerName := AQueryName;
  QueryType := AType;
end;

destructor VsQuery.Destroy;
begin

  inherited;
end;

procedure VsQuery.SetQuerName(const Value: String);
begin
  FQuerName := Value;
end;

procedure VsQuery.SetQueryType(const Value: TEnumQuery);
begin
  FQueryType := Value;
end;

{ VsTable }

constructor VsTable.Create(ATable: String);
begin
  FTable := ATable;
end;

destructor VsTable.Destroy;
begin

  inherited;
end;

procedure VsTable.SetTable(const Value: String);
begin
  FTable := Value;
end;

end.
